import re, argparse, json

# get the query
p = argparse.ArgumentParser()
p.add_argument("q")
query = vars(p.parse_args())["q"]

# load the word list for searching
def loadwords():
    with open("words.json") as wd:
        words = json.load(wd)
    return words

# clean the string of non a-z1-9 + space characters, provide some fallbacks
def clean(query: str):
    cquery = re.sub(r"[^a-z0-9\s]", "", query.lower().strip())
    if cquery in ["", "0"]:
        return "void"
    else:
        return cquery

# transform a query into a regex for searching
def transform(cquery: str):
    # split into blocks of strings based on type (alpha/digit/space)
    blocks = re.findall(r"[^\W\d]+|\d+|\s+", cquery)
    final = ""
    for part in blocks:
        if part.isdigit():
            if part != "0":
                part = "\w{%s}" % part
            else:
                part = ""
        final += part
    # one-liner to compress whitespace blocks
    final = " ".join(final.split())
    return final

# search the dictionary for word matches
def search(requery: str):
    requeries = requery.split(" ")
    wl = loadwords()
    final = ""
    for word in requeries:
        pattern = re.compile("^" + word + "$")
        words = list(filter(pattern.match, wl))
        if len(words) > 0:
            final += words[0] + " "
        else:
            final += "NOTFOUND" + " "
    return final[:-1]


# ===
print(search(transform(clean(query))))
